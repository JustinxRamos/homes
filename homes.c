#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "homes.h"

// readhomes function
home ** readhome(char * filename)
{
    int arrlen = 50;
    home **arr = (home **)malloc(arrlen * sizeof(home *));
    FILE *f = fopen(filename, "r");
    printf("file opened succesfully");
    if (!f)
    {
        printf("Can't open file %s\n", filename);
        exit(1);
    }
    //initialize variables/array   
    int zip = 0;
    char addr[55];
    int price = 0;
    int area = 0;
    int i = 0;
    //reads the file   
    while(fscanf(f, "%d,%49[^,],%d,%d", &zip,addr,&price,&area) != EOF)
    {
        arr[i] = malloc(sizeof(home));
        arr[i]->addr = (char*)malloc((strlen(addr)+1) * sizeof(char));
        strcpy(arr[i]->addr, addr);
        arr[i]->zip = zip;
        arr[i]->price = price;
        arr[i]->area = area;
        i++;
        //increases size of array if needed
        if (i == arrlen)
        {
            arrlen = (int)(arrlen + 100);
            home **newarr = realloc(arr, arrlen * sizeof(home *));
            if (newarr != NULL) arr = newarr;
            else exit(3);
        }
    }
    printf("Scan was successful!");
    //sets end of array to null
    arr[i] = NULL;
    return arr;
}
//Counts number of homes
int Homes_Count(home ** h)
{
    int i = 0;
    while ( h[i] != NULL )
    {
        i++;
    }
    return i;
}
//Gets number of homes that have specific zip code
int Get_Zip(int zip, home ** h)
{
    int i = 0;
    int count = 0;
    while (h[i] != NULL)
    {
        if (h[i]->zip == zip)
        {
            count++;
        }
        i++;
    }
    return count;
}
//prints the address and price of the homes
void Print(int price, home ** h, int range)
{
    int i = 0;
    while (h[i] != NULL)
    {
        if ((price >= h[i]->price - range) && (price <= h[i]->price + range))
        {
            printf("Address: %s, Price: %d\n", h[i]->addr, h[i]->price);
        }
        i++;
    }
}

int main(int argc, char *argv[])
{
    
    if (argc < 2)
    {
        printf( "Cant find file %s\n", argv[0]);
        exit(1);
    }
    home ** homes;
    homes = readhome(argv[1]);

    int Homes = Homes_Count(homes);
    printf("There are %d homes\n", Homes);
    
    // User enters zip code
    int zips = 0;
    printf("Please Enter a Zip code: ");
    scanf("%d", &zips);
    int homezip = Get_Zip(zips, homes);
    printf("There are %d homes in this zip code %d\n", homezip, zips);

    //Get the price
    int price = 0;
    printf("Please Enter a price:\n");
    scanf("%d", &price);

    //print 
    Print(price, homes, 10000);    
}
